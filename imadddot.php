<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			margin: 0px;
		}
		#image{
			max-width: 100%;
			width: 800px;

		}
		#imageIndex{
			font-size: 1.5em;

		}
		.dot{
			width: 10px;
            height: 10px;
            border-radius: 25px;
            position: absolute;
		}
	
	</style>
<?php
	session_start();
	if(isset($_SESSION["imagedir"])){
		$dir=$_SESSION["imagedir"];
		$arr=$_SESSION["arr"];
	}
 	if(isset($_SESSION["num"])){
 		$num=$_SESSION["num"];

 	}
 	$msg=$dir."/".$arr[($num-1)];
?>

</head>
<body>
	<div id="topdiv">
		<img src=<?=$msg?> id="image"/>
	</div>
	<input type="button" value="clear" onclick="removeAllDot()" />
	<span id="imageIndex"><?php echo $arr[($num-1)]." ; ".($num)."/".count($arr);  ?></span>
<!--	<input type="button" value="prev" onclick="prevImage()" />
	<input type="button" value="next"  onclick="nextImage()"/>-->
	
	<form id="postform" method="post" action="imrecord.php">
		<span id="myform"></span>
		<input type="hidden" name="imagedir" value=<?=$dir?>>
		<input type="hidden" name="num" value=<?=$num?>>
		<input type="hidden" id="degree" name="degree" value="" />
		<input id="submit" type="button" value="submit" name="submit" onclick="submitOpen()">
	</form>
	<div id="result"></div>
	<div><img id="previewImg" src="" style="width: 400px"></div>
	<div id="divid" background-color: #AAA;" >
		
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript">
		var num=<?php echo $num?>;
		var arr=<?php echo json_encode($arr)?>;
		var strfinal="";
		var strdefualt="";
		var dir=<?php echo json_encode($dir)?>;

		function add_select(obj)
		{
			
			var new_select = document.createElement("select");
			new_select.setAttribute("name","new_select[]");
			for (var i=0;i<arr.length;i++){
				var new_option = new Option(arr[i],arr[i]); 
				new_select.options.add(new_option);	
			}
			obj.append(new_select);
			return(new_select);			
		}
		
	

 
		$("#image").mousedown(function(e){
	
			var mousePos = {'x': e.offsetX, 'y': e.offsetY};
		 	console.log(mousePos);
		 	var dot= $('<div class="dot"></div>').css({
	                top: e.offsetY + 'px',
	                left: e.offsetX + 'px',
	          	  	background: '#FF0000'

	         })
		 	$("#topdiv").append(dot);   
		 	dot.append($("#topdiv>.dot").length);
       		calculateDegree(e.offsetX,e.offsetY);
       		add_select($("#myform"));
		});

		function submitOpen(){
			if($("#topdiv>.dot").length!=0){
				$("#submit").attr("type","submit");
			}
		}
		$("#myform").change(function(e){
			$("#previewImg").attr("src",dir+"/"+e.target.value);
		})

		function submitOpen(){
			$("#submit").attr("type","submit");
		}
		
		function removeAllDot(){
			$("#topdiv>.dot").remove();
			$("#myform>select").remove();

			strfinal="";
			$("#submit").attr("type","button");

		}


		function calculateDegree(x,y){
			var imgX=$("#image").width();
			var imgY=imgX/2;
			console.log(imgX+" "+imgY);
			var degreeX=Math.floor( (x-(imgX/2))/(imgX/360));
			var degreeY=Math.floor( -(y-(imgY/2))/(imgY/180));
			var str=degreeX+","+degreeY;
			console.log(str);
			if(strfinal!=""){
				strfinal=strfinal+","+str;
			}else{
				strfinal=strfinal+str;
			}	
			
			document.getElementById("divid").innerHTML=strfinal;
			$("#degree").attr("value",strfinal);

		} 
		
	</script>

</body>
</html>

